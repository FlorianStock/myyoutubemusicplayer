# My Youtube Music Player

This is a small personnal project developped on android studio. I wanted to be able to listen to music on youtube and be able to make playlists very quickly

App speed is very fast using only Youtube API requests without ads.

The library for API requests is [Ktor](https://ktor.io/) ;  it allowed me to retrieve a list of music on youtube with their identifiers. 
The other library that I used  for playing music is the [androidyoutubeplayer library](https://github.com/PierfrancescoSoffritti/android-youtube-player) from PierfrancescoSoffritti 

With a bit of UI development I managed to make a nice little app for personal use.

![image info](documentation/preview.jpg)

If you would to use this project, please change variable key and set your youtube API key in the  [YoutubeClient file](https://gitlab.com/FlorianStock/myyoutubemusicplayer/-/blob/main/app/src/main/java/fr/florianstock/myyoutubemusicplayer/api/YoutubeClient.kt?ref_type=heads)