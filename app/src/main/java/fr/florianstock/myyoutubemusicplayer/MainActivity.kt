package fr.florianstock.myyoutubemusicplayer


import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import fr.florianstock.myyoutubemusicplayer.api.dto.KindSearchResult
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.widget.ViewPager2
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import fr.florianstock.myyoutubemusicplayer.ui.components.ViewPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import fr.florianstock.myyoutubemusicplayer.ui.fragments.PlayListFragment
import fr.florianstock.myyoutubemusicplayer.ui.fragments.ReadingFragment
import fr.florianstock.myyoutubemusicplayer.ui.fragments.SearchMusicFragment

class MainActivity : AppCompatActivity() {

    lateinit var fragment :FragmentActivity
    var currentVideo:String? = null

    lateinit var readingFragment : ReadingFragment
    lateinit var searchMusicFragment : SearchMusicFragment

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

        fragment = this
        val viewPager : ViewPager2 = findViewById(R.id.viewpager)
        val tabLayout: TabLayout = findViewById(R.id.tab_layout)

        val youTubePlayerView = findViewById<YouTubePlayerView>(R.id.youtube_player_view)
        youTubePlayerView.enableBackgroundPlayback(true)
        youTubePlayerView.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {

            override fun onReady(youTubePlayer: YouTubePlayer) {
                val playListFragment = PlayListFragment()
                 readingFragment = ReadingFragment(object:ReadingFragment.ReadingEvents{
                    override fun playSound(musicId: String) {
                        youTubePlayer.loadVideo(musicId,0f)
                        currentVideo = musicId
                    }
                })
                searchMusicFragment = SearchMusicFragment(object : SearchMusicFragment.SearchEvents{
                    override fun addToReadingList(music: KindSearchResult) {
                        readingFragment.musicList.add(music)
                        readingFragment.adapterListMusic.notifyItemInserted(readingFragment.musicList.size)
                    }


                    override fun readMusic(musicId: String) {
                        youTubePlayer.loadVideo(musicId,0f)
                        currentVideo = musicId
                    }

                })

                val fragmentsViewPager: List<Fragment>  = listOf(searchMusicFragment,readingFragment,playListFragment)

                val adapterViewPager = ViewPagerAdapter(fragment,fragmentsViewPager)
                viewPager.adapter = adapterViewPager
                TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                    tab.text = adapterViewPager.getTitle(position)
                }.attach()
                //val videoId = "5m_flTU0OpA"
                //youTubePlayer.loadVideo(videoId, 0f)
            }

            override fun onStateChange(youTubePlayer: YouTubePlayer, state: PlayerConstants.PlayerState
            ) {
                when(state){
                    PlayerConstants.PlayerState.ENDED->{
                        val inPLayingList :Int = readingFragment.musicList.indexOfFirst { e -> e.id!!.videoId == currentVideo }
                        if(inPLayingList == -1){
                            if(readingFragment.musicList.size>0){
                                youTubePlayer.loadVideo(readingFragment.musicList[0].id!!.videoId!!,0f)
                                currentVideo = readingFragment.musicList[0].id!!.videoId!!
                            }
                        }else{
                            if(inPLayingList < readingFragment.musicList.size-1){
                                youTubePlayer.loadVideo(readingFragment.musicList[inPLayingList+1].id!!.videoId!!,0f)
                                currentVideo = readingFragment.musicList[inPLayingList+1].id!!.videoId!!
                            }else{
                                youTubePlayer.loadVideo(readingFragment.musicList[0].id!!.videoId!!,0f)
                                currentVideo = readingFragment.musicList[0].id!!.videoId!!
                            }
                        }
                    }
                }
                super.onStateChange(youTubePlayer, state)
            }
        })






    }




}

