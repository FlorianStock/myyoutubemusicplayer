package fr.florianstock.myyoutubemusicplayer.api.dto

import kotlinx.serialization.Serializable

@Serializable
data class ContentId(val kind: String?="",val videoId : String?="")