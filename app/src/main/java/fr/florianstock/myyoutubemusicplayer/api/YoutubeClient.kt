package fr.florianstock.myyoutubemusicplayer.api

import fr.florianstock.myyoutubemusicplayer.api.dto.KindSearchResult
import fr.florianstock.myyoutubemusicplayer.api.dto.SearchList
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*


object YoutubeClient
{
    val key : String = ""
    val baseUrl : String = "https://www.googleapis.com/youtube/v3"
    val client = HttpClient(CIO){
        install(JsonFeature) {
            serializer = KotlinxSerializer(json = kotlinx.serialization.json.Json {
                isLenient = true
                ignoreUnknownKeys = true
                explicitNulls = false
            })

        }
    }

    init{
       // val response: HttpResponse = client.get("https://ktor.io/")
    }

    suspend fun searchMusic(q : String) : List<KindSearchResult>
    {
        val res : SearchList  =  client.get("${baseUrl}/search"){
            parameter("key",key)
            parameter("part","snippet")
            parameter("categoryId","music")
            parameter("maxResults",8)
            parameter("q",q)
        }
        return res.items
    }


}