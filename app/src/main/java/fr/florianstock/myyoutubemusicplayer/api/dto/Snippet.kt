package fr.florianstock.myyoutubemusicplayer.api.dto

import kotlinx.serialization.Serializable

@Serializable
data class Snippet(
    val publishedAt:String,
    val title : String,
    val description : String,
    val thumbnails : Thumbnails,
    val channelTitle : String,
    val publishTime: String,
    val liveBroadcastContent: String
)


