package fr.florianstock.myyoutubemusicplayer.api.dto

import kotlinx.serialization.Serializable

@Serializable
data class Thumbnails(val default: Thumbnail, val medium: Thumbnail, val high: Thumbnail)
