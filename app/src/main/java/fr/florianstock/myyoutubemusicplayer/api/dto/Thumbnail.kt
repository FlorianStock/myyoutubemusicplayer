package fr.florianstock.myyoutubemusicplayer.api.dto

import kotlinx.serialization.Serializable

@Serializable
data class Thumbnail(
    val url :String="",
    val width: Int?,
    val height: Int?
)