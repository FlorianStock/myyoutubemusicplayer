package fr.florianstock.myyoutubemusicplayer.api.dto

import kotlinx.serialization.Serializable

@Serializable
data class KindSearchResult(
    val kind : String,
    val etag : String,
    val snippet : Snippet,
    val id : ContentId?
)