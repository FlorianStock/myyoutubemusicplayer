package fr.florianstock.myyoutubemusicplayer.api.dto

import kotlinx.serialization.*

@Serializable
data class SearchList(
    val items: List<KindSearchResult>,
    val nextPageToken : String
)