package fr.florianstock.myyoutubemusicplayer.ui.components

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.florianstock.myyoutubemusicplayer.R
import fr.florianstock.myyoutubemusicplayer.api.dto.KindSearchResult
import com.squareup.picasso.Picasso
import at.huber.youtubeExtractor.VideoMeta

import at.huber.youtubeExtractor.YtFile

import android.util.SparseArray

import at.huber.youtubeExtractor.YouTubeExtractor
import android.annotation.SuppressLint
import android.widget.ImageButton
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import fr.florianstock.myyoutubemusicplayer.ui.enums.MusicListType


class RecyclerViewMusic(private val listType: MusicListType, private val dataSet: ArrayList<KindSearchResult>) :
    RecyclerView.Adapter<RecyclerViewMusic.ViewHolder>() {

    lateinit var listener : ListMusicEvents

    interface ListMusicEvents{
        fun onClickEvent(pos:Int,music : KindSearchResult){}
        fun onPlayEvent(musicId: String){}
        fun onAddToReadingList(music : KindSearchResult){}
        fun removeItem(position:Int){}
    }

    public fun setEventListener(e: ListMusicEvents){
        listener = e
    }
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val musicName: TextView  = view.findViewById(R.id.music_name)
        val musicDescription: TextView  = view.findViewById(R.id.music_description)
        val musicImage : ImageView = view.findViewById(R.id.music_image)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        var layout = 0
        when(listType){
            MusicListType.ReadList-> layout = R.layout.row_reading
            MusicListType.SearchList-> layout = R.layout.row_search
            else -> R.layout.row_musiclist
        }
        val view = LayoutInflater.from(viewGroup.context).inflate(layout, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        if(listType == MusicListType.SearchList || listType == MusicListType.ReadList){
            val playBtn : ImageButton = viewHolder.itemView.findViewById(R.id.button_read)
            playBtn.setOnClickListener { listener.onPlayEvent(dataSet[position].id!!.videoId!!) }

            val playListBtn : ImageButton = viewHolder.itemView.findViewById(R.id.button_addToPlaylist)
            playListBtn.setOnClickListener {  }
        }

        when(listType) {
            MusicListType.SearchList->{
                val addToReadingBtn : ImageButton = viewHolder.itemView.findViewById(R.id.button_addToReadinglist)
                addToReadingBtn.setOnClickListener { listener.onAddToReadingList(dataSet[position]) }
            }
            MusicListType.ReadList->{
                val removeBtn : ImageButton = viewHolder.itemView.findViewById(R.id.button_remove)
                removeBtn.setOnClickListener { listener.removeItem(position) }
            }
        }

        viewHolder.itemView.setOnClickListener {
            val youtubeLink = "http://youtube.com/watch?v=${dataSet[position].id!!.videoId}"
            //extractYoutubeUrl(youtubeLink)
            listener.onClickEvent(position,dataSet[position])
        }

        viewHolder.musicName.text = dataSet[position].snippet.title
        viewHolder.musicDescription.text = dataSet[position].snippet.description

        Picasso.get()
            .load(dataSet[position].snippet.thumbnails.default.url)
            .resize(120,90).noFade().into(viewHolder.musicImage);


    }

    override fun getItemCount() = dataSet.size


}
