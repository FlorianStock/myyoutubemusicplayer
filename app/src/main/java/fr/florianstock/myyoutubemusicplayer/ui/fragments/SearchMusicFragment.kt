package fr.florianstock.myyoutubemusicplayer.ui.fragments

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.florianstock.myyoutubemusicplayer.R
import fr.florianstock.myyoutubemusicplayer.api.YoutubeClient
import fr.florianstock.myyoutubemusicplayer.api.dto.KindSearchResult
import fr.florianstock.myyoutubemusicplayer.ui.components.RecyclerViewMusic
import fr.florianstock.myyoutubemusicplayer.ui.enums.MusicListType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SearchMusicFragment(private val listener:SearchEvents) : Fragment()
{
    var musicList :ArrayList<KindSearchResult> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_listmusic,container,false)
    }

    override fun onViewCreated(view:View,savedInstanceState: Bundle?) {

        val search : EditText = view.findViewById(R.id.search)
        val searchLoupe : ImageButton = view.findViewById(R.id.searchbuton)
        val searchGo : ImageButton = view.findViewById(R.id.searchbuton_go)
        val listView :RecyclerView  = view.findViewById(R.id.MusicList)

        listView.layoutManager = LinearLayoutManager(context);
        val adapterListMusic = RecyclerViewMusic(MusicListType.SearchList,musicList)
        listView.adapter = adapterListMusic

        search.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if(search.text.toString() != ""){
                searchLoupe.visibility = View.GONE
                searchGo.visibility = View.VISIBLE
            }else{
                searchLoupe.visibility = View.VISIBLE
                searchGo.visibility = View.GONE
            }
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                CoroutineScope(Dispatchers.Main).launch {
                    musicList.clear()
                    musicList.addAll( ArrayList(YoutubeClient.searchMusic(search.text.toString())))
                    search.text.clear()
                    listView.adapter!!.notifyDataSetChanged()
                }
                return@OnKeyListener true
            }
            false
        })

        adapterListMusic.setEventListener(object :RecyclerViewMusic.ListMusicEvents{
            override fun onClickEvent(pos: Int, music: KindSearchResult) {
                listener.addToReadingList(music)
            }

            override fun onPlayEvent(musicId: String) {
                listener.readMusic(musicId)
            }

            override fun onAddToReadingList(music: KindSearchResult) {
                listener.addToReadingList(music)
            }

        })

    }

    interface SearchEvents{
        fun addToReadingList(music : KindSearchResult)
        fun readMusic(musicId : String)
    }

}