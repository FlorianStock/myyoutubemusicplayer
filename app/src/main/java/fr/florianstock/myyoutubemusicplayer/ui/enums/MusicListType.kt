package fr.florianstock.myyoutubemusicplayer.ui.enums

enum class  MusicListType {
    ReadList,
    SearchList,
    PlayList
}