package fr.florianstock.myyoutubemusicplayer.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import fr.florianstock.myyoutubemusicplayer.R
import fr.florianstock.myyoutubemusicplayer.api.dto.KindSearchResult
import fr.florianstock.myyoutubemusicplayer.ui.components.RecyclerViewMusic
import fr.florianstock.myyoutubemusicplayer.ui.enums.MusicListType

class ReadingFragment(private val listener : ReadingEvents) : Fragment() {

    var musicList :ArrayList<KindSearchResult> = ArrayList()
    var adapterListMusic :RecyclerViewMusic= RecyclerViewMusic(MusicListType.ReadList,musicList)

    interface ReadingEvents {
        fun playSound(musicId: String)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_listreading,container,false)
    }

    override fun onViewCreated(view:View,savedInstanceState: Bundle?) {

        val listView :RecyclerView  = view.findViewById(R.id.MusicList)
        listView.layoutManager = LinearLayoutManager(context);
        adapterListMusic = RecyclerViewMusic(MusicListType.ReadList,musicList)
        listView.adapter = adapterListMusic

        adapterListMusic.setEventListener(object:RecyclerViewMusic.ListMusicEvents {
            override fun onClickEvent(pos: Int, music: KindSearchResult) {
                listener.playSound(music.id!!.videoId!!)
            }

            override fun removeItem(position: Int) {
                musicList.removeAt(position)
                adapterListMusic.notifyItemRemoved(position)
                adapterListMusic.notifyItemRangeChanged(0, musicList.size);
            }

            override fun onPlayEvent(musicId: String) {
                listener.playSound(musicId)
            }
        })
    }
}