package fr.florianstock.myyoutubemusicplayer.ui.components

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class ViewPagerAdapter(fa: FragmentActivity,private val fragments:List<Fragment>) : FragmentStateAdapter(fa)  {
    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
      return fragments[position]
    }

    fun getTitle(position: Int):String
    {
        when(position){
            0 -> return "Recherche"
            1 -> return "Lecture"
            2 -> return "Playlist"
        }
        return ""
    }

}